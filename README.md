#My personal Sublime Text 2 cheatsheet

##Sections:
+ [Intro](#markdown-header-intro)
+ [Pre-Setup](#markdown-header-pre-setup)
+ [Useful-Shortcuts](#markdown-header-useful-shortcuts)
+ [Snippets](#markdown-header-snippets)
+ [Packages](#markdown-header-packages)

### Intro

This is my personal sublime text 2 cheatsheet that is intended to speed up my development process, and enhancing my programming skills. It is not a guide with explicit explanation, don't be confused.

#####Why sublime?
I have been using Coda for years as my official text editor for most of the time. I like the design, but that's about it. Coda slows down my pc, it's not that flexible in terms of code reuse. when I discovered sublime text 2, I don't want to go back again! I believe that no text editor out there can match the power of sublime! And you can see why!

Here is my small guide that I use on daily basis and I also encourage anyone reading this to take some time and do a small research about this amazing editor.

**(*)** Keep in mind I am using Mac so the shortcuts and commands will be slightly different for those who are on Linux and Windows.

**! IMPORTANT !** TO USE SOME OF THESE SHORTCUTS YOU NEED TO INSTALL SPECIFIC PACKAGES OR SNIPPETS.

* * *

### Pre-Setup

1. Setup a Theme in `Sublime Text 2 -> Preferences -> Color Scheme`, i use *Sunburst*
2. Open in Sublime 2 through right click

	**Set up using Automator** (default by mac by executing the following sequence)

	Service receives selected files or folder, in Finder.app

	Search for Open finder items

	Open with Sublime Text 2.app 

	Save Service as 'Open-in-Sublime'

3. **Open in terminal**

	Create a symbolic link like this

	`ln -s /Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl your$PATH` in my case it is `/usr/local/bin/sublime`

	To check, do `echo $PATH`

	Now, in terminal window within a directory i just type in `sublime . ` which opens up me entire folder in sublime.

* * *

### Useful Shortcuts 

`Cmd+D` => select a word, press more to select multiple occurrences of this word,

`Cmd+Shift+G` => place multiple cursors for all word occurrences,

`Cmd+Shift+L` => when having multiple lines selected, edit all of them

`Cmd+P` => quickly open file in current project folder and go (@)method,

`Cmd+R` => within a file quickly go to (@)method(symbol),

`Cmd+Shift+P` => Use to install packages, do specific things like adding snippets,

`Cmd+Alt+N` => Enter a path to a new file (current directory)

`Cmd+/` => Toggle comments

`Cmd+I` => Incremental search

`Cmd+Ctl+X` => Prefxr (removes unnecessary styling )

* * *

### Snippets

#### Useful Snippets (in my opinion)

1. JQuery snippet pack (JQuery useful snippets)
2. Blade snippets (PHP Laravel snippets)
3. Angular snippets (AngularJs snippets)
4. PHP unit testing snippet (For PHP testing)
5. Sass snippets ()

+ Look for more snippets in the package control that corresponds to your selected language, framework you are working with

### Packages

1. Emmet
2. Autoprefixer
3. SFTP
4. Advanced new file
5. Gist
6. Sublime Linter
7. Side bar